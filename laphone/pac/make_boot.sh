#!/bin/bash

curr=$(pwd -P)

if [[ $# -ge 1 ]]; then
package_path=$1
else
package_path=`cd ../../../../../out/laphone/packages/phone/images; pwd`
fi

basepath=$(cd `dirname $0`; pwd)

# some prebuild img here
work_path=$basepath/ImageFiles/

echo curr path: $curr
echo package path: $package_path
echo out path: $work_path

# boot.img
$basepath/mkbootimg \
    --os_version "13.0.0" --os_patch_level "2023-03" --header_version "4" \
    --cmdline "console=ttyS1,115200n8 buildvariant=userdebug" --kernel $basepath/kernel \
    --ramdisk $package_path/ramdisk.img \
    -o $work_path/boot.img

# boot-updater.img
$basepath/mkbootimg \
    --os_version "13.0.0" --os_patch_level "2023-03" --header_version "4" \
    --cmdline "console=ttyS1,115200n8 buildvariant=userdebug" --kernel $basepath/kernel \
    --ramdisk $package_path/updater.img \
    -o $work_path/boot-updater.img

# vendor_boot.img
$basepath/mkbootimg \
    --header_version 4 --pagesize 0x00001000 --base 0x00000000 \
    --kernel_offset 0x00008000 --ramdisk_offset 0x05400000 \
    --vendor_cmdline "console=ttyS1,115200n8 buildvariant=userdebug" \
    --dtb $basepath/dtb  --vendor_ramdisk $package_path/ramdisk.img \
    --vendor_boot $work_path/vendor_boot.img

# vendor_boot_updater.img
$basepath/mkbootimg \
    --header_version 4 --pagesize 0x00001000 --base 0x00000000 \
    --kernel_offset 0x00008000 --ramdisk_offset 0x05400000 \
    --vendor_cmdline "console=ttyS1,115200n8 buildvariant=userdebug" \
    --dtb $basepath/dtb  --vendor_ramdisk $package_path/updater.img \
    --vendor_boot $work_path/vendor_boot_updater.img
