#!/bin/bash

# Copyright (C) 2023 HiHope Open Source Organization.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e
set -v

curr=$(pwd -P)
basepath=$(cd `dirname $0`; pwd)

# args from build.sh or from bash execue
if [[ $# -ge 1 ]]; then
package_path=$1
else
package_path=`cd ../../../../../out/laphone/packages/phone/images; pwd`
fi

# some prebuild img here
work_path=$basepath/ImageFiles/

# we output the pac to this dest path
target_path=$package_path/laphone_nosec_userdebug.tar.gz
pac_path=$package_path/laphone_nosec_userdebug.pac


echo curr path: $curr
echo package path: $package
echo target path: $work_path

# del it first
if [[ -f "$target_path" ]]; then
    rm -f $target_path
fi

# cp image to work path
cp $package_path/*.img $work_path -rfv
# cp $package_path/*.bin $work_path

# go to work path and make pac
cd $work_path
perl ./../Script/mkpac.pl "$pac_path" "flash.cfg" "FlashParam"

tar -czf $target_path -C $package_path laphone_nosec_userdebug.pac

# check if pac is done
if [[ -f "$target_path" ]]; then
#    rm $pac_path
    echo -e "\033[32m  laphone build pac successful. \033[0m"
fi
